package com.wwddfd.perpersoneggdrop;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.World;
import org.bukkit.boss.DragonBattle;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerAdvancementDoneEvent;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import static org.bukkit.Material.*;

public class DragonKillListener implements Listener {
    private final PerPersonEggDrop plugin;

    public DragonKillListener(@NotNull PerPersonEggDrop plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR)
    private void OnAdvancement(PlayerAdvancementDoneEvent event) {
        final String killDragonKey = "end/kill_dragon";
        NamespacedKey advancementKey = event.getAdvancement().getKey();
        if(advancementKey.getKey().equals(killDragonKey)) {
            Player player = event.getPlayer();
            this.plugin.getLogger().info("[PerPersonEggDrop] Rewarding first dragon kill for " + player.getName());

            World currentWorld = player.getLocation().getWorld();
            DragonBattle battle = currentWorld.getEnderDragonBattle();
            if (battle != null) {
                if(!battle.hasBeenPreviouslyKilled()) {
                    this.plugin.getLogger().info("[PerPersonEggDrop] first kill for this server, letting the game place the egg");
                    return;
                }
                Location endPortalLocation = battle.getEndPortalLocation();
                if (endPortalLocation != null) {
                   int maxHeight = currentWorld.getMaxHeight();
                    searchColumn: for(int i = 4; i < maxHeight; ++i) {
                        Location loc = endPortalLocation.add(0, i, 0);
                        switch(loc.getBlock().getType()) {
                            case AIR:
                                this.plugin.getLogger().info("[PerPersonEggDrop] Replacing AIR with EGG at: " + loc.getBlockY());
                                loc.getBlock().setType(DRAGON_EGG);
                                break searchColumn;
                            //case DRAGON_EGG:
                            //    this.plugin.getLogger().info("[PerPersonEggDrop] EGG AT: " + loc.getBlockY());
                            //    break searchColumn;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }
}
