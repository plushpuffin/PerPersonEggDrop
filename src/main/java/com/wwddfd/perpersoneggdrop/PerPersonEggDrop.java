package com.wwddfd.perpersoneggdrop;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class PerPersonEggDrop extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic
        this.getLogger().info("[PerPersonEggDrop] Registering event listeners!");
        PluginManager manager = Bukkit.getPluginManager();
        manager.registerEvents(new DragonKillListener(this), this);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
